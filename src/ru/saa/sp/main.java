package ru.saa.sp;

/**
 * Класс для реализации умножения без оператора умножения
 *
 * @author A.Sidoruk 17IT18
 */
public class main {
    public static void main(String[] args) {
        int output = multiply(22, 3);
        System.out.println(output);
    }

    /**
     * Возвращает результат произведения firstValue на secondValue
     *
     * @param firstOperator  - множитель
     * @param secondOperator - множимое
     * @return - произведение
     */
    public static int multiply(int firstOperator, int secondOperator) {
        if (firstOperator == 0 || secondOperator == 0) {
            return 0;
        }

        boolean changeSign = firstOperator < 0 ^ secondOperator < 0 ? true : false;

        if (firstOperator < 0) {
            firstOperator = -firstOperator;
        }

        if (secondOperator < 0) {
            secondOperator = -secondOperator;
        }

        int multiplicand, reiteration;

        if (firstOperator > secondOperator) {
            multiplicand = firstOperator;
            reiteration = secondOperator;
        } else {
            multiplicand = secondOperator;
            reiteration = firstOperator;
        }

        int composition = 0;

        if (reiteration % 2 != 0) {
            reiteration -= 1;
            composition = multiplicand;
        }

        int TwoMultiplicand = multiplicand + multiplicand;

        for (int i = 0; i < reiteration / 2; i++) {
            composition += TwoMultiplicand;
        }

        if (changeSign == true) {
            composition = -composition;
        }

        return composition;
    }
}